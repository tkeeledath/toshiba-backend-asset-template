var uuid = require('node-uuid');
var async = require('async');

function generateUUID() {
    return uuid.v1();
}

function buildSqlStringIds(arrayIDs) {
    var resultStr = '';
    arrayIDs.forEach(function (oId) {
        if (resultStr === '') {
            resultStr = resultStr + "'" + oId + "'";
        } else {
            resultStr = resultStr + ",'" + oId + "'";
        }
    });
    return resultStr;
}

function retrieveSubRecursive(
    connection,
    accountIDs,
    subAccounts,
    fields,
    user_id,
    cb,
) {
    if (accountIDs.length === 0) {
        return cb(null, subAccounts);
    }

    var sql =
        'SELECT ' +
        fields +
        ' FROM fndy_account a ' +
        'LEFT JOIN fndy_account pa ON pa.id = a.parent_id ' +
        'WHERE a.parent_id IN (' +
        buildSqlStringIds(accountIDs) +
        ')';
    if (user_id) {
        sql =
            'SELECT ' +
            fields +
            ' FROM (fndy_account a, fndy_user u) ' +
            'LEFT JOIN fndy_account pa ON pa.id = a.parent_id ' +
            'LEFT JOIN fndy_account_acl acl ON a.id = acl.account_id ' +
            'WHERE a.parent_id IN (' +
            buildSqlStringIds(accountIDs) +
            ') AND u.id = "' +
            user_id +
            '" ' +
            conditionForAcl;
    }

    connection.query(sql, function (err, rows) {
        if (err) {
            return cb(err);
        }
        if (rows.length === 0) {
            return cb(null, subAccounts);
        }

        var childIDs = [];
        rows.forEach(function (child) {
            childIDs.push(child.id);
            subAccounts.push(child);
        });
        return retrieveSubRecursive(
            connection,
            childIDs,
            subAccounts,
            fields,
            user_id,
            cb,
        );
    });
}

function retrieveSubRecursiveWithoutRoot(
    connection,
    accountIDs,
    subAccounts,
    cb,
) {
    if (accountIDs.length === 0) {
        return cb(null, subAccounts);
    }

    var sql =
        'SELECT a.id FROM fndy_account a ' +
        'LEFT JOIN fndy_account pa ON pa.id = a.parent_id ' +
        'WHERE a.parent_id IN (' +
        buildSqlStringIds(accountIDs) +
        ') AND a.is_root_account = 0';

    connection.query(sql, function (err, rows) {
        if (err) {
            return cb(err);
        }
        if (rows.length === 0) {
            return cb(null, subAccounts);
        }

        var childIDs = [];
        rows.forEach(function (child) {
            childIDs.push(child.id);
            subAccounts.push(child.id);
        });
        return retrieveSubRecursiveWithoutRoot(
            connection,
            childIDs,
            subAccounts,
            cb,
        );
    });
}

function getSubAccountIdsWithoutRoot(connection, pAccountID, cb) {
    retrieveSubRecursiveWithoutRoot(
        connection,
        [pAccountID],
        [pAccountID],
        function (err, subAccounts) {
            if (err) {
                return cb(err);
            }

            return cb(null, subAccounts);
        },
    );
}

function retrieveParentRootRecursive(connection, accountID, cb) {
    var sql =
        'SELECT parent_id,  CAST(is_root_account AS UNSIGNED) AS is_root_account  FROM fndy_account  WHERE id="' +
        accountID +
        '"';

    connection.query(sql, function (err, rows) {
        //console.log(rows);
        if (err) {
            return cb(err);
        }
        if (rows.length === 0) {
            return cb(null, null);
        }
        if (rows[0].is_root_account === 1) {
            return cb(null, accountID);
        }
        if (!rows[0].parent_id) {
            return cb(null, null);
        }

        return retrieveParentRootRecursive(
            connection,
            rows[0].parent_id,
            cb,
        );
    });
}

function getAccountHierarchy(
    connection,
    pAccountID,
    fields,
    user_id,
    cb,
) {
    function init(cbInit) {
        return cbInit(null, connection, pAccountID, fields);
    }

    function getParentAccount(
        con,
        pAccountID,
        desiredFields,
        cbParentAccount,
    ) {
        var fields =
            'a.id AS id, a.name AS name, a.address AS address, a.city AS city, a.state AS state, ' +
            'a.country AS country, a.postal_code AS postal_code, a.phone_number AS phone_number, ' +
            'CAST(a.active AS UNSIGNED) AS aActive, ' +
            'a.read_endpoint AS read_endpoint, a.write_endpoint AS write_endpoint, a.created_date AS created_date, ' +
            'a.parent_id AS parent_id, pa.name AS parent_name, a.password_policy AS password_policy';

        if (desiredFields && desiredFields.length > 0) {
            var accountFields = desiredFields.split(',');
            var customFields = '';

            accountFields.forEach(function (key) {
                var handleKey = '';
                switch (key) {
                    case 'id':
                        handleKey = 'a.id AS id';
                        break;
                    case 'name':
                        handleKey = 'a.name AS name';
                        break;
                    case 'parent_id':
                        handleKey =
                            'a.parent_id AS parent_id, pa.name AS parent_name';
                        break;
                    case 'address':
                        handleKey = 'a.address AS address';
                        break;
                    case 'city':
                        handleKey = 'a.city AS city';
                        break;
                    case 'state':
                        handleKey = 'a.state AS state';
                        break;
                    case 'country':
                        handleKey = 'a.country AS country';
                        break;
                    case 'postal_code':
                        handleKey = 'a.postal_code AS postal_code';
                        break;
                    case 'phone_number':
                        handleKey = 'a.phone_number AS phone_number';
                        break;
                    case 'active':
                        handleKey =
                            'CAST(a.active AS UNSIGNED) AS aActive';
                        break;
                    case 'read_endpoint':
                        handleKey =
                            'a.read_endpoint AS read_endpoint';
                        break;
                    case 'write_endpoint':
                        handleKey =
                            'a.write_endpoint AS write_endpoint';
                        break;
                    case 'created_date':
                        handleKey = 'a.created_date AS created_date';
                        break;
                    case 'password_policy':
                        handleKey =
                            'a.password_policy AS password_policy';
                        break;
                }

                if (customFields === '' && handleKey) {
                    customFields = customFields + handleKey;
                } else {
                    customFields = customFields + ', ' + handleKey;
                }
            });

            fields = customFields;
        }

        var sql =
            'SELECT ' +
            fields +
            ' FROM fndy_account a ' +
            'LEFT JOIN fndy_account pa ON pa.id = a.parent_id ' +
            'WHERE a.id = "' +
            pAccountID +
            '"';

        if (user_id) {
            sql =
                'SELECT ' +
                fields +
                ' FROM (fndy_account a, fndy_user u) ' +
                'LEFT JOIN fndy_account pa ON pa.id = a.parent_id ' +
                'LEFT JOIN fndy_account_acl acl ON a.id = acl.account_id ' +
                'WHERE a.id = "' +
                pAccountID +
                '" AND u.id = "' +
                user_id +
                '" ' +
                conditionForAcl;
        }

        con.query(sql, function (err, rows) {
            if (err) {
                return cbParentAccount(err);
            }
            if (!rows || rows.length === 0) {
                return cbParentAccount('Account not found');
            }

            cbParentAccount(null, con, rows[0], fields);
        });
    }

    function getSubAccounts(con, aParent, fields, cbSubAccounts) {
        retrieveSubRecursive(
            con,
            [aParent.id],
            [aParent],
            fields,
            user_id,
            function (err, subAccounts) {
                if (err) {
                    return cbSubAccounts(err);
                }
                cbSubAccounts(null, con, fields, subAccounts);
                //cbSubAccounts(null, subAccounts);
            },
        );
    }

    function getUserSharedAccounts(
        con,
        fields,
        subAccounts,
        cbGetSharedAccounts,
    ) {
        getSharedAccounts(con, fields, user_id, function (err, rows) {
            subAccounts = subAccounts.concat(rows);

            subAccounts = subAccounts.filter(
                (a, index, self) =>
                    self.findIndex((t) => t.id === a.id) === index,
            );

            cbGetSharedAccounts(null, subAccounts);
        });
    }

    async.waterfall(
        [
            foundryUtils.writeToAuditLog,
            init,
            getParentAccount,
            getSubAccounts,
            getUserSharedAccounts,
        ],
        function (err, result) {
            if (err) {
                return cb(err);
            }
            cb(null, result);
        },
    );
}

function getAccountHierarchyIds(connection, pAccountID, user_id, cb) {
    getAccountHierarchy(
        connection,
        pAccountID,
        'id',
        user_id,
        function (err, accounts) {
            if (err) {
                return cb(err);
            }
            var accountIDs = [];
            accounts.forEach(function (a) {
                accountIDs.push(a.id);
            });
            cb(null, accountIDs);
        },
    );
}

function getAccountTree(
    connection,
    accountID,
    user_id,
    response_type,
    cbGetAccountTree,
) {
    function init(callbackInit) {
        return callbackInit(null, connection, accountID);
    }

    function searchMyAccount(
        con,
        accountID,
        callbackSearchMyAccount,
    ) {
        var sql =
            'SELECT a.id, a.name, a.parent_id, pa.name AS parent_name, a.city,a.state,a.address,a.country,a.postal_code ' +
            'FROM fndy_account a ' +
            'LEFT JOIN fndy_account pa on pa.id = a.parent_id ' +
            'WHERE a.id = "' +
            accountID +
            '"';

        con.query(sql, function (err, accounts) {
            if (err || accounts.length === 0) {
                return callbackSearchMyAccount('Account not found');
            }
            callbackSearchMyAccount(null, con, accounts[0]);
        });
    }

    function getSuperAndSubAccounts(
        con,
        myAccount,
        callbackGetSuperAndSubAccounts,
    ) {
        var fields =
            'a.id, a.name, a.parent_id, pa.name AS parent_name';
        if (response_type == 'object') {
            //for advanced search. get all accounts in one request.
            fields +=
                ', a.city,a.state,a.address,a.country,a.postal_code';
        }
        async.parallel(
            {
                superA: getSuperAccount.bind(
                    null,
                    con,
                    myAccount.parent_id,
                    [],
                    user_id,
                ),
                subA: retrieveSubRecursive.bind(
                    null,
                    con,
                    [myAccount.id],
                    [myAccount],
                    fields,
                    user_id,
                ),
                shared: getSharedAccounts.bind(
                    null,
                    con,
                    fields,
                    user_id,
                ),
            },
            function (err, cbResultAccounts) {
                if (err) {
                    return callbackGetSuperAndSubAccounts(err);
                }
                var resultAccounts;
                if (response_type !== 'object') {
                    resultAccounts = [];

                    if (
                        cbResultAccounts.superA &&
                        cbResultAccounts.superA.length > 0
                    ) {
                        resultAccounts = resultAccounts.concat(
                            cbResultAccounts.superA,
                        );
                    }
                    if (
                        cbResultAccounts.subA &&
                        cbResultAccounts.subA.length > 0
                    ) {
                        resultAccounts = resultAccounts.concat(
                            cbResultAccounts.subA,
                        );
                    }

                    if (
                        cbResultAccounts.shared &&
                        cbResultAccounts.shared.length > 0
                    ) {
                        resultAccounts = resultAccounts.concat(
                            cbResultAccounts.shared,
                        );
                        resultAccounts = resultAccounts.filter(
                            (a, index, self) =>
                                self.findIndex(
                                    (t) => t.id === a.id,
                                ) === index,
                        );
                    }
                } else {
                    resultAccounts = {};

                    resultAccounts['parents'] =
                        cbResultAccounts.superA;
                    resultAccounts['sub_accounts'] =
                        cbResultAccounts.subA;
                    resultAccounts['shared'] =
                        cbResultAccounts.shared;
                }

                callbackGetSuperAndSubAccounts(null, resultAccounts);
            },
        );
    }

    async.waterfall(
        [init, searchMyAccount, getSuperAndSubAccounts],
        function (err, result) {
            if (err) {
                return cbGetAccountTree(err);
            }
            cbGetAccountTree(null, result);
        },
    );
}

function getSuperAccount(
    con,
    parentID,
    superAccounts,
    user_id,
    cbGetSuperAccount,
) {
    if (!parentID) {
        return cbGetSuperAccount(null, superAccounts);
    }

    var sql =
        'SELECT a.id, a.name, a.parent_id, pa.name AS parent_name ' +
        'FROM fndy_account a ' +
        'LEFT JOIN fndy_account pa ON pa.id = a.parent_id ' +
        'WHERE a.id = "' +
        parentID +
        '"';
    if (user_id) {
        sql =
            'SELECT a.id, a.name, a.parent_id, pa.name AS parent_name ' +
            'FROM (fndy_account a, fndy_user u) ' +
            'LEFT JOIN fndy_account pa ON pa.id = a.parent_id ' +
            'LEFT JOIN fndy_account_acl acl ON a.id = acl.account_id ' +
            'WHERE a.id = "' +
            parentID +
            '" AND u.id = "' +
            user_id +
            '" ' +
            conditionForAcl;
    }

    con.query(sql, function (err, accounts) {
        if (err || accounts.length === 0) {
            return cbGetSuperAccount(null, superAccounts);
        }

        var superA = accounts[0];
        superAccounts.unshift(superA);

        return getSuperAccount(
            con,
            superA.parent_id,
            superAccounts,
            user_id,
            cbGetSuperAccount,
        );
    });
}

function getSharedAccounts(
    con,
    fields,
    user_id,
    cbGetSharedAccounts,
) {
    if (!user_id) return cbGetSharedAccounts(null, []);

    const sql =
        'SELECT ' +
        fields +
        ' FROM (fndy_account a, fndy_user u) ' +
        'LEFT JOIN fndy_account pa ON pa.id = a.parent_id ' +
        'LEFT JOIN fndy_account_acl acl ON a.id = acl.account_id ' +
        'WHERE u.id="' +
        user_id +
        '" AND ' +
        '(acl.grantee_account_id=u.account_id ' +
        ' OR acl.grantee_user_id = u.id)';

    con.query(sql, function (err, rows) {
        if (err) {
            return cbGetSharedAccounts(null, []);
        }
        if (!rows || rows.length === 0) {
            return cbGetSharedAccounts(null, []);
        }

        cbGetSharedAccounts(null, rows);
    });
}

function getAclForAccountIdWithPermission(
    con,
    account_id,
    required_permission,
    cbCheckAccessToAccountForUser,
) {
    var sql =
        'SELECT * ' +
        'FROM fndy_account_acl ' +
        'WHERE account_id="' +
        account_id +
        '"';
    if (required_permission && required_permission.length) {
        sql += ' AND ' + required_permission + '=1';
    }
    con.query(sql, function (err, acls) {
        if (err || acls.length === 0) {
            return cbCheckAccessToAccountForUser(false);
        }

        return cbCheckAccessToAccountForUser(true, acls);
    });
}

var conditionForAcl =
    'AND (a.id = u.account_id ' +
    'OR ((acl.grantee_role = "Any" ' +
    'OR acl.grantee_role = u.role) ' +
    'AND (acl.grantee_account_id = u.account_id ' +
    'OR acl.grantee_user_id = u.id ' +
    'OR (acl.grantee_account_id IS NULL ' +
    'AND acl.grantee_user_id IS NULL)))) GROUP BY a.id';

function CheckAccessToAccount(
    con,
    account_id,
    user_id,
    required_permission,
    cbCheckAccessToAccount,
) {
    function getUser(cbGetUser) {
        var sql =
            'SELECT CAST(active AS UNSIGNED) AS active, account_id, id, role FROM fndy_user WHERE id="' +
            user_id +
            '"';

        con.query(sql, function (err, res) {
            if (err) {
                return cbGetUser(err);
            }

            if (res && res.length === 0) {
                return cbGetUser('User not found');
            }

            cbGetUser(null, res[0]);
        });
    }

    function CheckAcl(identityUser, cbCheckAcl) {
        if (
            required_permission === 'write_accounts' ||
            required_permission === 'delete_accounts'
        ) {
            return cbCheckAcl(null, identityUser, false);
        }

        getAclForAccountIdWithPermission(
            con,
            account_id,
            required_permission,
            function (isAccessible, acls) {
                //console.log(acls);
                if (!isAccessible) return cbCheckAcl('Access denied');

                const granteeAcls = acls.filter(
                    (a) =>
                        a.grantee_user_id === identityUser.id ||
                        a.grantee_account_id ===
                            identityUser.account_id,
                );
                if (granteeAcls.length) {
                    return cbCheckAcl(null, identityUser, true);
                }

                const anyAcls = acls.filter(
                    (a) =>
                        a.grantee_user_id === null &&
                        a.grantee_account_id === null &&
                        (a.grantee_role === 'Any' ||
                            a.grantee_role === identityUser.role),
                );
                if (!anyAcls.length)
                    return cbCheckAcl('Access denied');

                return cbCheckAcl(null, identityUser, false);
            },
        );
    }

    function retrieveParentRecursive(accountID, parentAccountID, cb) {
        var sql =
            'SELECT parent_id  FROM fndy_account  WHERE id="' +
            accountID +
            '"';

        con.query(sql, function (err, rows) {
            //console.log(rows);
            if (err) {
                return cb(err);
            }
            if (rows.length === 0 || !rows[0].parent_id) {
                return cb(null, false);
            }
            if (rows[0].parent_id === parentAccountID) {
                return cb(null, true);
            }
            return retrieveParentRecursive(
                rows[0].parent_id,
                parentAccountID,
                cb,
            );
        });
    }

    function checkParentAccount(
        identityAccount,
        grantee,
        cbCheckParentAccount,
    ) {
        console.log('GET CHECK ACCOUNT > CHECK PARENT ACCOUNT');

        if (grantee || account_id === identityAccount.account_id) {
            return cbCheckParentAccount(null, true);
        }

        retrieveParentRecursive(
            account_id,
            identityAccount.account_id,
            function (err, data) {
                if (err) return cbCheckParentAccount(err);
                cbCheckParentAccount(null, data);
            },
        );
    }

    async.waterfall(
        [getUser, CheckAcl, checkParentAccount],
        function (err, result) {
            if (err) {
                return cbCheckAccessToAccount(false);
            }

            return cbCheckAccessToAccount(result);
        },
    );
}

module.exports = {
    generateUUID: generateUUID,
    getAccountHierarchy: getAccountHierarchy,
    getAccountHierarchyIds: getAccountHierarchyIds,
    buildSqlStringIds: buildSqlStringIds,
    getAccountTree: getAccountTree,
    conditionForAcl: conditionForAcl,
    getAclForAccountIdWithPermission: getAclForAccountIdWithPermission,
    CheckAccessToAccount: CheckAccessToAccount,
    getSubAccountIdsWithoutRoot: getSubAccountIdsWithoutRoot,
    retrieveParentRootRecursive: retrieveParentRootRecursive,
};
