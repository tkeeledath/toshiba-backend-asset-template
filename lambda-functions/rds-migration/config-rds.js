var env = process.env.NODE_ENV;
console.log(env);

var env_configs = {
    dev: {
        RDS_CONNECT: {
            host:
                'backupdev.co1mxrtopg8x.us-east-1.rds.amazonaws.com',
            user: 'foundry',
            password: '23BcDevFNDy00',
            port: '3306',
            database: 'foundry',
        },
        FOUNDRY_HOST: 'http://foundry.thinglogix.com',
    },
    stage: {
        RDS_CONNECT: {
            host:
                'foundrystage.co1mxrtopg8x.us-east-1.rds.amazonaws.com',
            user: 'foundry',
            password: '4Her5mLjLbr',
            port: '3306',
            database: 'foundry',
        },
        FOUNDRY_HOST: 'http://stage.thinglogix.com',
    },
    prod: {
        RDS_CONNECT: {
            host: 'foundry.co1mxrtopg8x.us-east-1.rds.amazonaws.com',
            user: 'foundry',
            password: 'foundryrds',
            port: '3306',
            database: 'foundry',
        },
        FOUNDRY_HOST: 'http://foundry.thinglogix.com',
    },
};

var config = {
    ENV: env,
    AWS_REGION: 'us-west-2',
    RDS_CONNECT: env_configs[env].RDS_CONNECT,
    FOUNDRY_HOST: env_configs[env].FOUNDRY_HOST,
};

module.exports = config;
