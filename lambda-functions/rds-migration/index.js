var config = require('./config-rds');
var utils = require('./rds-utils');
var async = require('async');
var mysql = require('mysql');
var sql_bricks = require('sql-bricks-postgres');
let rdsDao = require('foundry-dao/rds')();

exports.handler = function (event, context) {
    console.log('MIGRATE TO PG > EVENT: ', event);
    var ACCOUNT_ID = event.body.account_id; // "c7b7bad0-c36c-11e6-b522-eb62b73d3b20";
    let env = 'dev';
    if (typeof process.env.ENV === 'string') {
        env = process.env.ENV.toLowerCase();
    }

    // TODO check datetime fields in RDS data  on value "0000-00-00 00:00:00". It is invalid value for Postgresql
    // TODO configure Postgresql password

    // connect to TL RDS
    var rds_config = config.RDS_CONNECT;
    // function for convert BIT type from Buffer to '0' or '1'
    // and replace invalid for PG characters
    rds_config.typeCast = function castField(
        field,
        useDefaultTypeCasting,
    ) {
        if (field.type === 'BIT' && field.length === 1) {
            var bytes_bit = field.buffer();
            return bytes_bit[0] === 1 ? '1' : '0';
        } else if (field.type === 'VARCHAR') {
            var bytes = field
                .buffer()
                .toString('utf8')
                .replace(/\\/g, 'WUBWUBREALSLASHWUB')
                .replace(/\\'/g, "''")
                .replace(/WUBWUBREALSLASHWUB/g, '\\');
            return bytes;
        }
        return useDefaultTypeCasting();
    };
    var connection = mysql.createConnection(rds_config);

    // connect to Postgresql

    var migratedIDs = {
        fndy_account: [],
        fndy_user: [],
    };
    function query(str, callback) {
        rdsDao.query(str, function (err, data) {
            // console.log(data);
            if (!err) {
                console.log(data);
                return callback(null, data);
            } else {
                console.log(err);
                console.log(str);
                return callback(err, data);
            }
        });
    }

    function InitPG(cbInit) {
        console.log('MIGRATE TO PG > INIT PG');

        rdsDao = require('foundry-dao/rds')(
            {
                host: process.env.DB_HOST,
                user: 'search_user',
                port: 5432,
                database: 'AdvancedSearch',
            },
            process.env.AWS_REGION || 'eu-central-1',
            env,
        );
        console.log('here');
        rdsDao.connect((err) => {
            console.log(err);
            cbInit(err);
        });
    }

    // // create tables and types in PG from file DB_schema.psql
    // function InstallDBschema(cbInstallDBschema) {
    //     console.log('MIGRATE TO PG > InstallDBschema');
    //     var schema = fs.readFileSync('./DB_schema.psql').toString('utf-8');
    //     query(schema, function(err) {
    //         if (err) {
    //             console.log(err);
    //             return cbInstallDBschema(err);
    //         }
    //         console.log(err);
    //         cbInstallDBschema(null);
    //     });
    // }

    function Init(cbInit) {
        console.log('MIGRATE MySQL TO PG > INIT');

        cbInit(null);
    }

    function ConnectRds(cbConnectRds) {
        console.log('MIGRATE TO PG > CONNECT RDS');

        connection.connect(function (err) {
            if (err) {
                return cbConnectRds(err);
            }

            cbConnectRds(null);
        });
    }

    function GetAccountIds(cbGetAccountIds) {
        console.log('MIGRATE TO PG > GET ACCOUNT IDS');

        utils.getAccountTree(
            connection,
            ACCOUNT_ID,
            null,
            'object',
            function (err, res) {
                if (err) {
                    return cbGetAccountIds(err);
                }
                console.log(res);
                var accountIDs = [];
                res['sub_accounts'].forEach(function (child) {
                    accountIDs.push(child.id);
                });

                var parentsAccountIDs = [];
                res['parents'].forEach(function (child) {
                    parentsAccountIDs.push(child.id);
                });
                migratedIDs.fndy_account = accountIDs;
                cbGetAccountIds(null, parentsAccountIDs);
            },
        );
    }

    function migrateAccounts(parentsAccountIDs, cbMigrateAccounts) {
        console.log('MIGRATE TO PG > migrateAccounts');
        var accounts = parentsAccountIDs.concat(
            migratedIDs.fndy_account,
        );
        var sql =
            `SELECT 
        id,
        name,
        parent_id,
        picture,
        address,
        city,
        state,
        country,
        postal_code,
        phone_number,
        active,
        read_endpoint,
        write_endpoint,
        created_date,
        created_date AS updated_date,
        mfa,
        password_policy,
        is_root_account,
        sfdc_instance_type,
        sfdc_access_token,
        sfdc_refresh_token,
        mfa_expiration_days
        ` +
            'FROM fndy_account WHERE id IN (' +
            utils.buildSqlStringIds(accounts) +
            ')';

        connection.query(sql, function (err, res) {
            if (err) {
                return cbMigrateAccounts(err);
            }
            // not needed because utils.getAccountHierarchy check count of ids
            if (res && res.length === 0) {
                return cbMigrateAccounts('Account not found');
            }
            console.log(res.length);

            query(
                sql_bricks
                    .insertInto('fndy_account')
                    .onConflict('id')
                    .doUpdate()
                    .values(res)
                    .toString(),
                function (err) {
                    if (err) {
                        console.log(err);
                        return cbMigrateAccounts(err);
                    }

                    cbMigrateAccounts(null);
                },
            );
        });
    }

    function getAndMigrateUsers(cbGetAndMigrateUsers) {
        console.log('MIGRATE TO PG > GET USERS');

        var sql =
            'SELECT * FROM fndy_user WHERE account_id IN (' +
            utils.buildSqlStringIds(migratedIDs.fndy_account) +
            ')';
        console.log(sql);

        connection.query(sql, function (err, rows) {
            if (err) {
                return cbGetAndMigrateUsers(err);
            }
            if (!rows || rows.length === 0) {
                return cbGetAndMigrateUsers(null);
            }
            var userIDs = [];
            rows.forEach(function (child) {
                userIDs.push(child.id);
            });
            console.log(rows.length);
            migratedIDs.fndy_user = userIDs;

            query(
                sql_bricks
                    .insertInto('fndy_user')
                    .onConflict('id')
                    .doUpdate()
                    .values(rows)
                    .toString(),
                function (err) {
                    if (err) {
                        console.log(err);
                        return cbGetAndMigrateUsers(err);
                    }

                    cbGetAndMigrateUsers(null);
                },
            );
        });
    }

    function migrateByAccountId(cbMigrateByAccountId) {
        console.log('MIGRATE TO PG > migrateByAccountId');
        var tables = ['fndy_custom_page', 'fndy_portal_page'];
        async.mapSeries(
            tables,
            function (table, callback) {
                var sql =
                    'SELECT * ' +
                    'FROM ' +
                    table +
                    ' WHERE account_id IN (' +
                    utils.buildSqlStringIds(
                        migratedIDs.fndy_account,
                    ) +
                    ')';

                connection.query(sql, function (err, res) {
                    if (err) {
                        return callback(err);
                    }

                    if (res && res.length === 0) {
                        return callback(null);
                    }
                    console.log(res.length);

                    query(
                        sql_bricks
                            .insertInto(table)
                            .onConflict('id')
                            .doUpdate()
                            .values(res)
                            .toString(),
                        function (err) {
                            if (err) {
                                console.log(err);
                                return callback(err);
                            }

                            callback(null);
                        },
                    );
                });
            },
            function (err) {
                if (err) {
                    console.log(err);
                    return cbMigrateByAccountId(err);
                }

                cbMigrateByAccountId(null);
            },
        );
    }

    function migrateByUserId(cbMigrateByUserId) {
        console.log('MIGRATE TO PG > migrateByUserId');
        var tables = [
            'fndy_mqtt_config',
            'fndy_mqtt_publish',
            'fndy_mqtt_subscribe',
            'fndy_verification_code',
        ];
        async.mapSeries(
            tables,
            function (table, callback) {
                var sql =
                    'SELECT * ' +
                    'FROM ' +
                    table +
                    ' WHERE user_id IN (' +
                    utils.buildSqlStringIds(migratedIDs.fndy_user) +
                    ')';

                connection.query(sql, function (err, res) {
                    if (err) {
                        return callback(err);
                    }

                    if (res && res.length === 0) {
                        return callback(null);
                    }
                    console.log(res.length);

                    query(
                        sql_bricks
                            .insertInto(table)
                            .onConflict('id')
                            .doUpdate()
                            .values(res)
                            .toString(),
                        function (err) {
                            if (err) {
                                console.log(err);
                                return callback(err);
                            }

                            callback(null);
                        },
                    );
                });
            },
            function (err) {
                if (err) {
                    console.log(err);
                    return cbMigrateByUserId(err);
                }

                cbMigrateByUserId(null);
            },
        );
    }

    function updateForAcl(cbUpdateForAcl) {
        console.log('MIGRATE TO PG > updateForAcl');
        var sql =
            'SELECT * ' +
            'FROM fndy_account_acl WHERE account_id IN (' +
            utils.buildSqlStringIds(migratedIDs.fndy_account) +
            ')';

        connection.query(sql, function (err, res) {
            if (err) {
                return cbUpdateForAcl(err);
            }

            if (res && res.length === 0) {
                return cbUpdateForAcl(null);
            }
            console.log(res.length);
            var acl = [];
            res.forEach(function (child) {
                if (
                    !(
                        (child.grantee_account_id != null &&
                            !migratedIDs.fndy_account.includes(
                                child.grantee_account_id,
                            )) ||
                        (child.grantee_user_id != null &&
                            !migratedIDs.fndy_user.includes(
                                child.grantee_user_id,
                            ))
                    )
                ) {
                    acl.push(child);
                }
            });

            console.log(acl.length);
            query(
                sql_bricks
                    .insertInto('fndy_account_acl')
                    .values(acl)
                    .onConflict('id')
                    .doUpdate()
                    .toString(),
                function (err) {
                    if (err) {
                        console.log(err);
                        return cbUpdateForAcl(err);
                    }

                    cbUpdateForAcl(null);
                },
            );
        });
    }

    async.waterfall(
        [
            Init,
            InitPG,
            // InstallDBschema,
            ConnectRds,
            GetAccountIds,
            migrateAccounts,
            getAndMigrateUsers,
            migrateByAccountId,
            migrateByUserId,
            updateForAcl,

            // GetDataFromMySQL
        ],
        function (err, result) {
            if (connection !== undefined) {
                connection.destroy();
            }
            rdsDao.closeConnection();

            if (err) {
                console.error('error:', err);
                var response = {
                    status: 400,
                    error: err.message ? err.message : err,
                };
                return context.fail(JSON.stringify(response));
            }

            return context.succeed(result);
        },
    );
};
