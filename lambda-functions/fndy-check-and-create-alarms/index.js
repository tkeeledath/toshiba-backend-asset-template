let env = 'dev';

if (
    typeof process.env.ENV === 'string' &&
    (process.env.ENV.toLowerCase() === 'prod' ||
        process.env.ENV.toLowerCase() === 'stage')
) {
    env = process.env.ENV.toLowerCase();
}

const asyncc = require('async');
const moment = require('moment');
const AWS = require('aws-sdk');

const https = require('https');

const dbConfig = {
    region: process.env.AWS_REGION,
    httpOptions: {
        agent: new https.Agent({
            ciphers: 'ALL',
            secureProtocol: 'TLSv1_method',
        }),
    },
};

const dynamoDB = require('@awspilot/dynamodb')(dbConfig);

function DataNormalizationForDynamoDB(data) {
    let resultData = null;

    try {
        const stringData = JSON.stringify(data);
        const stringDataWithoutBlankString = stringData.replace(
            /:""/g,
            ':null',
        );
        resultData = JSON.parse(stringDataWithoutBlankString);
    } catch (err) {
        console.error(
            'FOUNDRY DYNAMO DB DAO UTILS > DATA NORMALIZATION FOR DYNAMO DB ERROR: ',
            data,
            err,
        );
    }

    return resultData;
}

function AccumulateDailyReportData(
    deviceId,
    historyRecord,
    cbAccumulateDailyReportData,
) {
    const startDate = moment().startOf('day').valueOf();
    dynamoDB
        .table(`toshiba_daily_history_${env}`)
        .where('deviceId')
        .eq(deviceId)
        .where('epochTime')
        .eq(startDate)
        .get((err, result) => {
            const combinedHistory = !err && result ? result : {};

            const unnecessaryKeys = [
                'a.node.ups.serial',
                'a.contact.address.zip',
                'a.publishinterval',
                'a.node.emd.events',
                'epochTime',
                'a.contact.phoneext',
                'a.node.ups.events',
            ];

            Object.keys(historyRecord).forEach((key) => {
                if (
                    !Number.isNaN(historyRecord[key]) &&
                    unnecessaryKeys.indexOf(key) === -1
                ) {
                    combinedHistory[key] =
                        +(combinedHistory[key] || 0) +
                        +historyRecord[key];
                    combinedHistory[`${key}_count`] =
                        (combinedHistory[`${key}_count`] || 0) + 1;
                    if (
                        !combinedHistory[`${key}_min`] &&
                        combinedHistory[`${key}_min`] !== 0
                    )
                        combinedHistory[
                            `${key}_min`
                        ] = +historyRecord[key];
                    if (
                        !combinedHistory[`${key}_max`] &&
                        combinedHistory[`${key}_max`] !== 0
                    )
                        combinedHistory[
                            `${key}_max`
                        ] = +historyRecord[key];
                    combinedHistory[`${key}_min`] = Math.min(
                        combinedHistory[`${key}_min`],
                        +historyRecord[key],
                    );
                    combinedHistory[`${key}_max`] = Math.max(
                        combinedHistory[`${key}_max`],
                        +historyRecord[key],
                    );
                }
            });

            combinedHistory.epochTime = startDate;
            combinedHistory.deviceId = deviceId;
            const normalizedData = DataNormalizationForDynamoDB(
                combinedHistory,
            );
            if (normalizedData !== null) {
                dynamoDB
                    .table(`toshiba_daily_history_${env}`)
                    .insert_or_update(normalizedData, (insertErr) => {
                        if (insertErr) {
                            console.log(insertErr, deviceId);
                            return cbAccumulateDailyReportData(null);
                        }

                        return cbAccumulateDailyReportData(null);
                    });
            } else {
                console.log('Data is not valid');
                cbAccumulateDailyReportData(null);
            }
        });
}

function removeOldEventsFromNew(newEvents, oldEvents) {
    oldEvents.forEach((oldEvent) => {
        for (let i = 0; i < newEvents.length; i += 1) {
            if (
                oldEvent.event === newEvents[i].event &&
                oldEvent.epochtime === newEvents[i].epochtime &&
                oldEvent.type === newEvents[i].type
            ) {
                newEvents.splice(i, 1);
                break;
            }
        }
    });
}

function CheckAlarm(device, simpleJson, cbCheckAlarm) {
    console.log('RULE UPDATE FROM TOPIC > CHECK ALARM');

    function getNewEvents(key, nodeType) {
        let newEventObjects = [];
        let oldEventObjects = [];

        if (
            simpleJson[key] &&
            simpleJson[key].length &&
            simpleJson[key] !== device[key]
        ) {
            try {
                newEventObjects = JSON.parse(simpleJson[key]);
            } catch (e) {
                newEventObjects = [];
            }
            try {
                oldEventObjects = JSON.parse(device[key]);
            } catch (e) {
                oldEventObjects = [];
            }

            newEventObjects = newEventObjects.map((event) => {
                const toUpdate = { ...event };
                toUpdate.node = nodeType;
                return toUpdate;
            });

            removeOldEventsFromNew(newEventObjects, oldEventObjects);
        }
        return newEventObjects;
    }

    const events = getNewEvents('a.node.ups.events', 'ups').concat(
        getNewEvents('a.node.emd.events', 'emd'),
    );

    if (events.length) {
        const lambda = new AWS.Lambda({
            region: process.env.AWS_REGION,
        });

        const params = {
            FunctionName: `fndy-toshiba-events-processing-${env}`,
            InvocationType: 'Event',
            Payload: JSON.stringify({
                device: simpleJson,
                events,
            }),
        };

        lambda.invoke(params, () => cbCheckAlarm(null));
    } else {
        cbCheckAlarm(null);
    }
}

function handler(event, context) {
    const { oldObject, object } = event;

    const decodedOld = JSON.parse(Buffer.from(oldObject, 'base64'));
    const decodedNew = JSON.parse(Buffer.from(object, 'base64'));

    asyncc.waterfall(
        [
            (cb) => {
                AccumulateDailyReportData(
                    decodedOld.deviceId,
                    decodedOld,
                    cb,
                );
            },
            (cb) => {
                CheckAlarm(decodedOld, decodedNew, cb);
            },
        ],
        (err, result) => {
            if (err) {
                return context.fail(err);
            }

            return context.succeed(result);
        },
    );
}

exports.handler = handler;
