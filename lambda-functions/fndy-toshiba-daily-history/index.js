let env = 'dev';

if (
    typeof process.env.ENV === 'string' &&
    (process.env.ENV.toLowerCase() === 'prod' ||
        process.env.ENV.toLowerCase() === 'stage')
) {
    env = process.env.ENV.toLowerCase();
}
const https = require('https');
const region = process.env.AWS_REGION || "us-west-2";

const dao = require('foundry-dao/dynamodb')(
    region,
    '_' + env
);
let db_config = {
    region: region,
    httpOptions: {
        agent: new https.Agent({
            ciphers: 'ALL',
            secureProtocol: 'TLSv1_method',
        })
    }
};
const dynamoDB = require('@awspilot/dynamodb')(db_config);
const async = require('async');
const moment = require('moment');

exports.handler = (event, context) => {
    const typeId = '84bf2c70-ff4f-11e7-a278-770f8c02f05e';
    const tableName = `toshiba_daily_history_${env}`;

    console.log(event);

    function GetDevices(cbGetDevices) {
        console.log('DAILY HISTORY > GET DEVICES');

        dao.deviceInfo.GetForType(typeId, (err, devices) => {
            cbGetDevices(err, devices);
        });
    }

    function DataNormalizationForDynamoDB(data) {
        let resultData = null;

        try {
            const stringData = JSON.stringify(data);
            const stringDataWithoutBlankString = stringData.replace(
                /:""/g,
                ':null',
            );
            resultData = JSON.parse(stringDataWithoutBlankString);
        } catch (err) {
            console.error(
                'FOUNDRY DYNAMO DB DAO UTILS > DATA NORMALIZATION FOR DYNAMO DB ERROR: ',
                data,
                err,
            );
        }

        return resultData;
    }

    function GetDailyHistory(devices, cbGetDailyHistory) {
        console.log(
            `DAILY HISTORY > GET HISTORY FOR ${devices.length} DEVICES`,
        );

        if (Object.keys(event).indexOf('daysAgo') < -1) {
            const tempEvent = event;
            tempEvent.daysAgo = 1;
        }
        const yesterday = moment().subtract(event.daysAgo, 'days');
        const startDate = yesterday.startOf('day').valueOf();
        const endDate = yesterday.endOf('day').valueOf();

        console.log(startDate, endDate);

        async.mapSeries(
            devices,
            (device, cbMap) => {
                dao.deviceHistory.GetHistoryByRange(
                    'mqtt',
                    device.deviceId,
                    startDate,
                    endDate,
                    // eslint-disable-next-line consistent-return
                    (err, history) => {
                        if (err) {
                            console.log(err, device.deviceId);
                            return cbMap(null);
                        }
                    })
                });
                Object.keys(combinedHistory).forEach(key => {
                    if (!isNaN(combinedHistory[key]) && (!key.endsWith('_min') && !key.endsWith('_max'))) {
                        combinedHistory[key] = combinedHistory[key]/history.length;
                    }
                });
                combinedHistory.epochTime = startDate;
                let normalizedData = DataNormalizationForDynamoDB(combinedHistory);
                if (normalizedData !== null) {
                    dynamoDB
                        .table(tableName)
                        .insert_or_update(normalizedData, function(err) {
                            if (err) {
                                console.log(err, device.deviceId);
                                return cbMap(null);
                            }

                            cbMap(null);

                        });
                } else {
                    console.log('Data is not valid');
                    cbMap(null);
                }

            }, {select: null})
        }, cbGetDailyHistory);


    }

    async.waterfall([GetDevices, GetDailyHistory], (err) => {
        if (err) {
            console.error('error:', err);
            const response = {
                status: 400,
                error: err.message ? err.message : err,
            };
            return context.succeed(JSON.stringify(response));
        }

        return context.succeed({});
    });
};
