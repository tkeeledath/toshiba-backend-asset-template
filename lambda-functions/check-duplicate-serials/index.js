const dao = require('foundry-dao/dynamodb')(
    process.env.AWS_REGION,
    `_${process.env.ENV}`,
);
const util = require('util');

function getDevicesByType(typeId) {
    return util.promisify(dao.deviceInfo.GetForType)(typeId);
}

async function handler(event) {
    const { fndyObject } = event;

    try {
        const devices = await getDevicesByType(fndyObject.typeId);

        const duplicates = devices.filter(
            (d) => d['a.serial'] === fndyObject['a.serial'],
        );

        if (duplicates.length) {
            return Promise.reject(
                new Error(
                    `Device with serial number: ${fndyObject['a.serial']} already exists`,
                ),
            );
        }

        return Promise.resolve({
            message: 'Success',
        });
    } catch (e) {
        return Promise.reject(e);
    }
}

exports.handler = handler;
