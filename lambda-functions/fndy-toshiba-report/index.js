const AWS = require('aws-sdk');
const moment = require('moment');
const async = require('async');
const btoa = require('btoa');

const https = require('https');

const region = process.env.AWS_REGION || 'us-west-2';
let env = 'dev';

if (
    typeof process.env.ENV === 'string' &&
    (process.env.ENV.toLowerCase() === 'prod' ||
        process.env.ENV.toLowerCase() === 'stage')
) {
    env = process.env.ENV.toLowerCase();
}
const dao = require('foundry-dao/dynamodb')(
    process.env.AWS_REGION || 'eu-central-1',
    `_${env}`,
);

const dbConfig = {
    region,
    httpOptions: {
        agent: new https.Agent({
            ciphers: 'ALL',
            secureProtocol: 'TLSv1_method',
        }),
    },
};
const dynamoDB = require('@awspilot/dynamodb')(dbConfig);

exports.handler = (event, context) => {
    const typeId = '84bf2c70-ff4f-11e7-a278-770f8c02f05e';

    const histTableName = `toshiba_daily_history_${env}`;

    let fileName = '';

    function GetDevices(cbGetDevices) {
        console.log('REPORT > GET DEVICES');

        if (event.body.deviceId) {
            dao.deviceInfo.GetOne(
                event.body.deviceId,
                (err, device) => {
                    cbGetDevices(err, [device]);
                },
            );
        } else {
            dao.deviceInfo.GetForType(typeId, (err, devices) => {
                cbGetDevices(err, devices);
            });
        }
    }

    function GetHistoryByRange(deviceId, start, end, cbGetHistory) {
        let devices = [];
        (function recursiveCall($lastKey) {
            dynamoDB
                .table(histTableName)
                .where('deviceId')
                .eq(deviceId)
                .where('epochTime')
                .between(start, end)
                .resume($lastKey)
                // eslint-disable-next-line func-names, consistent-return
                .query(function (err, result) {
                    if (err) {
                        return cbGetHistory(err);
                    }
                    devices = devices.concat(result);

                    if (this.LastEvaluatedKey === null) {
                        return cbGetHistory(null, devices);
                    }
                    const $this = this;
                    setTimeout(() => {
                        recursiveCall($this.LastEvaluatedKey);
                    }, 0);
                });
        })(null);
    }

    function GetValueBeforeHistory(
        deviceId,
        end,
        cbGetValueBeforeHistory,
    ) {
        dynamoDB
            .table(`fndy_object_history_posted_${env}`)
            .where('deviceId')
            .eq(deviceId)
            .where('epochTime')
            .lt(end)
            .desc()
            .limit(1)
            .query((err, result) => {
                if (err || !result.length) {
                    return cbGetValueBeforeHistory(err);
                }
                return cbGetValueBeforeHistory(null, result[0]);
            });
    }

    function GetEventsForDevice(deviceId, start, end, cbGetAlarms) {
        let events = [];
        (function recursiveCall($lastKey) {
            dynamoDB
                .table(`toshiba_events_${env}`)
                .where('deviceId')
                .eq(deviceId)
                .where('epochTime')
                .between(start, end)
                .resume($lastKey)
                // eslint-disable-next-line func-names, consistent-return
                .query(function (err, result) {
                    if (err) {
                        return cbGetAlarms(null, events);
                    }
                    events = events.concat(result);

                    if (this.LastEvaluatedKey === null) {
                        return cbGetAlarms(null, events);
                    }
                    const $this = this;
                    setTimeout(() => {
                        recursiveCall($this.LastEvaluatedKey);
                    }, 0);
                });
        })(null);
    }

    function populateHtml(
        html,
        device,
        averageData,
        events,
        pageBreak,
    ) {
        console.log('The devices are:-');
        console.log(device);
        let address = 'n/a';
        let resultHTML = html;
        if (device['a.contact.address.street']) {
            address = `${device['a.contact.address.street'] || ''} ${
                device['a.contact.address.city'] || ''
            }, ${device['a.contact.address.state'] || ''} ${
                device['a.contact.address.zip'] || ''
            }`.toUpperCase();
        }
        resultHTML += `<div style='page-break-after: always'>
                <h3>${device['a.name']} | ${
            device['a.node.ups.model'] || 'n/a'
        } | S/N ${device['a.node.ups.serial'] || 'n/a'}</h3>
                <h5>Location: ${address}</h5>`;
        const summaryData = [
            {
                title: 'Output Voltage',
                key: 'a.node.ups.output.voltage1',
            },
            {
                title: 'Output Voltage Phase A',
                key: 'a.node.ups.output.voltage1',
            },
            {
                title: 'Output Voltage Phase B',
                key: 'a.node.ups.output.voltage2',
            },
            {
                title: 'Output Voltage Phase C',
                key: 'a.node.ups.output.voltage3',
            },
            {
                title: 'Input Voltage',
                key: 'a.node.ups.input.voltage1',
            },
            {
                title: 'Input Voltage Phase A',
                key: 'a.node.ups.input.voltage1',
            },
            {
                title: 'Input Voltage Phase B',
                key: 'a.node.ups.input.voltage2',
            },
            {
                title: 'Input Voltage Phase C',
                key: 'a.node.ups.input.voltage3',
            },
            {
                title: 'Input Frequency',
                key: 'a.node.ups.input.frequency1',
            },
            {
                title: 'Output Frequency',
                key: 'a.node.ups.output.frequency1',
            },
            {
                title: 'Output Load %',
                key: 'a.node.ups.output.loadpercentage',
            },
            {
                title: 'Output Load % Phase A',
                key: 'a.node.ups.output.current1percent',
            },
            {
                title: 'Output Load % Phase B',
                key: 'a.node.ups.output.current2percent',
            },
            {
                title: 'Output Load % Phase C',
                key: 'a.node.ups.output.current3percent',
            },
            {
                title: 'Battery Voltage',
                key: 'a.node.ups.battery.voltage',
            },
            { title: 'Temperature', key: 'a.node.emd.temperature' },
            {
                title: 'EMD Humidity',
                key: 'a.node.emd.humiditypercent',
            },
        ];
        const deviceName1 = '1600';
        const deviceName2 = 'T1000';
        const deviceName3 = '5000 3P1';
        if (device['a.node.ups.name']) {
            if (
                device['a.node.ups.name'].indexOf(deviceName1) !==
                    -1 ||
                device['a.node.ups.name'].indexOf(deviceName2) !==
                    -1 ||
                device['a.node.ups.name'].indexOf(deviceName3) !== -1
            ) {
                // get index of object with title
                const removeIndex1 = summaryData
                    .map((item) => item.title)
                    .indexOf('Output Voltage Phase A');
                summaryData.splice(removeIndex1, 1);
                const removeIndex2 = summaryData
                    .map((item) => item.title)
                    .indexOf('Output Voltage Phase B');
                summaryData.splice(removeIndex2, 1);
                const removeIndex3 = summaryData
                    .map((item) => item.title)
                    .indexOf('Output Voltage Phase C');
                summaryData.splice(removeIndex3, 1);
                const removeIndex4 = summaryData
                    .map((item) => item.title)
                    .indexOf('Input Voltage Phase A');
                summaryData.splice(removeIndex4, 1);
                const removeIndex5 = summaryData
                    .map((item) => item.title)
                    .indexOf('Input Voltage Phase B');
                summaryData.splice(removeIndex5, 1);
                const removeIndex6 = summaryData
                    .map((item) => item.title)
                    .indexOf('Input Voltage Phase C');
                summaryData.splice(removeIndex6, 1);
                // remove object

                console.log(summaryData);
            } else {
                const removeIndex7 = summaryData
                    .map((item) => item.title)
                    .indexOf('Output Voltage');
                summaryData.splice(removeIndex7, 1);
                const removeIndex8 = summaryData
                    .map((item) => item.title)
                    .indexOf('Input Voltage');
                summaryData.splice(removeIndex8, 1);

                console.log(summaryData);
            }
            console.log('Final data for summary');
            console.log(summaryData);
        }

        resultHTML += `<table class="table table-striped">
                    <thead><tr><th>Parameter</th><th>Min</th><th>Avg</th><th>Max</th></tr></thead><tbody>`;
        const resultAverageData = averageData;
        summaryData.forEach((row) => {
            if (resultAverageData[`${row.key}_max`] === undefined)
                resultAverageData[`${row.key}_max`] = +(
                    resultAverageData[row.key] || 0
                );
            if (resultAverageData[`${row.key}_min`] === undefined)
                resultAverageData[`${row.key}_min`] = +(
                    resultAverageData[row.key] || 0
                );
            resultHTML += `<tr>
                            <td>${row.title}</td>
                            <td>${Math.ceil(
                                resultAverageData[`${row.key}_min`] ||
                                    0,
                            )}</td>
                            <td>${Math.ceil(
                                ((resultAverageData[
                                    `${row.key}_min`
                                ] || 0) +
                                    (resultAverageData[
                                        `${row.key}_max`
                                    ] || 0)) /
                                    2,
                            )}</td>
                            <td>${Math.ceil(
                                resultAverageData[`${row.key}_max`] ||
                                    0,
                            )}</td>
                        </tr>`;
        });

        resultHTML += `</tbody></table></div>`;

        resultHTML += `<div ${
            pageBreak ? "style='page-break-after: always'" : ''
        }>
                <h3>Alarms</h3>
                <div style="font-weight: bold;margin-bottom:10px;">
                    <div style="width:200px;display:inline-block">Total Faults: ${
                        events.filter((e) => e.type === 'fault')
                            .length
                    }</div>
                    <div style="width:200px;display:inline-block">Total Warnings: ${
                        events.filter((e) => e.type === 'warning')
                            .length
                    }</div>
                    <div style="width:200px;display:inline-block">Total Informationals: ${
                        events.filter((e) => e.type === 'information')
                            .length
                    }</div>
                </div>`;

        resultHTML += `<table class="table table-striped">
                    <thead><tr><th>Severity</th><th>Time</th><th>Message</th></tr></thead><tbody>`;

        console.log('Events', events);
        const severity = {
            info: 'Informational',
            information: 'Informational',
            warning: 'Warning',
            fault: 'Fault',
            critical: 'Critical',
        };
        events.forEach((eventItem) => {
            console.log('Event', eventItem);
            resultHTML += `<tr>
                        <td>${severity[eventItem.type]}</td>
                        <td>${moment(eventItem.epochTime)
                            .utcOffset(-6)
                            .format('MM/DD/YYYY hh:mm A Z')}</td>
                        <td>${eventItem.node.toUpperCase()}: ${
                eventItem.event
            }</td>
                    </tr>`;
        });

        resultHTML += `</tbody></table></div>`;

        return resultHTML;
    }

    function PopulateReport(devices, cbPopulateReport) {
        console.log('REPORT > POPULATE REPORT');

        let startDate = moment(event.body.startTime)
            .startOf('day')
            .valueOf();
        let endDate = moment(event.body.endTime)
            .endOf('day')
            .valueOf();
        if (event.body.interval) {
            endDate = moment().endOf('day').valueOf();
            switch (event.body.interval) {
                case 'week':
                default:
                    startDate = moment()
                        .subtract(1, 'weeks')
                        .startOf('day')
                        .valueOf();
                    break;
                case 'day':
                    startDate = moment()
                        .subtract(1, 'days')
                        .startOf('day')
                        .valueOf();
                    break;
                case 'month':
                    startDate = moment()
                        .subtract(1, 'months')
                        .startOf('day')
                        .valueOf();
                    break;
            }
        }

        let html = `<html>
        <style type = 'text/css'>
            table {border-spacing: 0; border-collapse: collapse;}
            table {background-color: transparent;}
            .table {width: 100%; max-width: 100%;}
            .table > tbody > tr {text-align: left; line-height: 26px; vertical-align: middle; border-bottom: 1px solid black;}
            .table > thead > tr > th {text-align: left; line-height: 26px; vertical-align: middle; border-bottom: 1px solid black;}
            .table > thead {display: table-header-group;}
            /*// .table-striped > tbody > tr:nth-of-type(even) {background-color: #fafbfc;}*/
            table.table tr {
                    page-break-inside: avoid!important; }
            table.blue {width: 100%; max-width: 100%;}        
            table.blue > thead > tr {background-color:blue;color:white;font-weight: bold;}
            table.blue > tbody > tr:nth-of-type(even) {background-color: #DDDDDD;}
            table.blue > tbody > tr {border-left:2px solid black; border-right:2px solid black;}
            table.blue > tbody > tr > td {padding-left:10px;}
            </style></head>`;

        html =
            `${html}<body><div style='page-break-inside:avoid !important'>` +
            `<h3>Devices</h3><table class='table table-striped' style='page-break-after: always;font-size:12px;page-break-inside:avoid;'>`;

        html =
            `${html}<thead><tr>` +
            `<th>Model</th>` +
            `<th>Serial Number</th>` +
            `<th>UPS Size</th>` +
            `<th>Location</th>` +
            `</tr></thead><tbody>`;

        devices.forEach((device) => {
            if (device.Created > endDate) {
                return false;
            }
            let address = 'n/a';
            if (device['a.contact.address.street']) {
                address = `${
                    device['a.contact.address.street'] || ''
                } ${device['a.contact.address.city'] || ''}, ${
                    device['a.contact.address.state'] || ''
                } ${
                    device['a.contact.address.zip'] || ''
                }`.toUpperCase();
            }
            html += `<tr>
                <td>${device['a.node.ups.model'] || 'n/a'}</td>
                <td>${device['a.node.ups.serial'] || 'n/a'}</td>
                <td>${device['a.node.ups.capacity'] || 'n/a'}</td>
                <td>${address}</td>
                </tr>`;
            return null;
        });

        html += '</tbody></table></div>';

        console.log(startDate, endDate);
        fileName = `report-${moment(startDate).format(
            'MM-DD-YYYY',
        )}-${moment(endDate).format(
            'MM-DD-YYYY',
        )}-${moment().unix()}`;

        let countOfPages = 0;

        async.mapSeries(
            devices,
            // eslint-disable-next-line consistent-return
            (device, cbMap) => {
                if (device.Created > endDate) {
                    countOfPages += 1;
                    return cbMap(null);
                }

                GetHistoryByRange(
                    device.deviceId,
                    startDate,
                    endDate,
                    (err, history) => {
                        let averageData = {};
                        if (!err && history.length) {
                            // if (moment().isSame(moment(endDate), 'day')) history.push(device);
                            GetValueBeforeHistory(
                                device.deviceId,
                                startDate,
                                (errGetHistoryByRange, record) => {
                                    let finalRecord = record;
                                    if (
                                        errGetHistoryByRange ||
                                        !finalRecord
                                    ) {
                                        finalRecord = device;
                                    }

                                    Object.keys(finalRecord).forEach(
                                        (key) => {
                                            if (
                                                !Number.isNaN(
                                                    finalRecord[key],
                                                )
                                            ) {
                                                finalRecord[
                                                    `${key}_min`
                                                ] = +finalRecord[key];
                                                finalRecord[
                                                    `${key}_max`
                                                ] = +finalRecord[key];
                                            }
                                        },
                                    );
                                    history.push(finalRecord);
                                    history.forEach((hist) => {
                                        Object.keys(hist).forEach(
                                            (key) => {
                                                if (
                                                    key.endsWith(
                                                        '_min',
                                                    )
                                                ) {
                                                    if (
                                                        !averageData[
                                                            key
                                                        ] &&
                                                        averageData[
                                                            key
                                                        ] !== 0
                                                    )
                                                        averageData[
                                                            key
                                                        ] = +hist[
                                                            key
                                                        ];
                                                    averageData[
                                                        key
                                                    ] = Math.min(
                                                        averageData[
                                                            key
                                                        ],
                                                        +hist[key],
                                                    );
                                                } else if (
                                                    key.endsWith(
                                                        '_max',
                                                    )
                                                ) {
                                                    if (
                                                        !averageData[
                                                            key
                                                        ] &&
                                                        averageData[
                                                            key
                                                        ] !== 0
                                                    )
                                                        averageData[
                                                            key
                                                        ] = +hist[
                                                            key
                                                        ];
                                                    averageData[
                                                        key
                                                    ] = Math.max(
                                                        averageData[
                                                            key
                                                        ],
                                                        +hist[key],
                                                    );
                                                }
                                            },
                                        );
                                    });

                                    GetEventsForDevice(
                                        device.deviceId,
                                        startDate,
                                        endDate,
                                        (_err, events) => {
                                            countOfPages += 1;
                                            html = populateHtml(
                                                html,
                                                device,
                                                averageData,
                                                events,
                                                countOfPages <
                                                    devices.length,
                                            );
                                            cbMap(null);
                                        },
                                    );
                                },
                            );
                        } else {
                            averageData = device;
                            GetEventsForDevice(
                                device.deviceId,
                                startDate,
                                endDate,
                                (_err, events) => {
                                    countOfPages += 1;
                                    html = populateHtml(
                                        html,
                                        device,
                                        averageData,
                                        events,
                                        countOfPages < devices.length,
                                    );
                                    cbMap(null);
                                },
                            );
                        }
                    },
                );
            },
            () => {
                html += `</body></html>`;
                cbPopulateReport(null, html);
            },
        );
    }

    function UploadFileToS3(html, cbUploadFileToS3) {
        console.log('REPORT > UPLOAD REPORT');

        const lambda = new AWS.Lambda();

        const payload = {
            body: {
                html_base64: btoa(html),
                fileName,
                // options: pdfOptions,
                options: {
                    pageSize: 'letter',
                    headerHtml: `https://thinglogix-customer-files.s3.amazonaws.com/toshiba/header-first.html`,
                    headerSpacing: 10,
                    marginTop: 60,
                    marginBottom: 5,
                    // marginLeft: 40,
                    // marginRight: 40,
                },
            },
            headers: {},
            method: 'GET',
            params: {},
            query: {},
            invoke: true,
        };

        const params = {
            FunctionName: `fndy-upload-report-to-s3-${env}`,
            InvocationType: 'RequestResponse',
            Payload: JSON.stringify(payload),
        };
        lambda.invoke(params, (err, data) => {
            if (err) {
                console.log(err, err.message);
                return cbUploadFileToS3(err.message);
            }
            console.log(data);
            return cbUploadFileToS3(null, JSON.parse(data.Payload));
        });
    }

    async.waterfall(
        [GetDevices, PopulateReport, UploadFileToS3],
        (err, result) => {
            if (err) {
                console.error('error:', err);
                const response = {
                    status: 400,
                    error: err.message ? err.message : err,
                };
                return context.fail(JSON.stringify(response));
            }
            console.log(result);
            return context.succeed(result);
        },
    );
};
