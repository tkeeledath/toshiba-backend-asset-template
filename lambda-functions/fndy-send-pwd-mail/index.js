/* eslint-disable func-names */
const async = require('async');
const AWS = require('aws-sdk');

const DEFAULT_PWD = 'Toshiba1';
const ses = new AWS.SES({
    region: process.env.SES_REGION
        ? process.env.SES_REGION
        : 'us-west-2',
});
exports.handler = function (event, context, callback) {
    console.log('event', event);

    function validation(cbValidation) {
        if (event.body) {
            // eslint-disable-next-line no-param-reassign
            event = event.body;
        }
        if (!event.email) {
            return cbValidation('User email not available');
        }
        return cbValidation(null);
    }

    function sendEmail(cbSendEmail) {
        const body = {};
        if (process.env.FROM_EMAIL) {
            body.from_email = process.env.FROM_EMAIL;
        }

        if (process.env.SUBJECT_EMAIL) {
            body.subject_email = process.env.SUBJECT_EMAIL;
        }

        const message =
            `<p>
                Hi
                <strong
                    >${
                        event.userName ? event.userName : 'User'
                    }</strong
                >,
            </p>` +
            `Here is the password for your new account:<br/>` +
            `<p>
            NOTE: Your
            password is
            <strong
                >${DEFAULT_PWD}</strong
            >
            </p>
            <p>
                If you have any
                questions, you can
                reach out to us at
                <a
                    href="mailto:TIC-PED-TMS@Toshiba.com"
                    >TIC-PED-TMS@Toshiba.com</a
                >.
            </p>`;

        const mail = {
            Destination: {
                ToAddresses: [event.email],
            },
            Message: {
                Body: {
                    Html: {
                        Charset: 'UTF-8',
                        Data: message,
                    },
                },
                Subject: {
                    Charset: 'UTF-8',
                    Data:
                        body.subject_email ||
                        'Password For Toshiba Monitoring System',
                },
            },
            Source:
                body.from_email ||
                '"Toshiba Monitoring System " <TIC-PED-TMS@Toshiba.com>',
        };

        ses.sendEmail(mail)
            .promise()
            .then(() => {
                cbSendEmail(null, body);
            })
            .catch((err) => {
                cbSendEmail(err);
            });
    }

    async.waterfall([validation, sendEmail], function (err, data) {
        if (err) {
            const response = {
                error: err.error ? err.error : err,
            };
            callback(JSON.stringify(response), null);
        } else {
            callback(null, data);
        }
    });
};
