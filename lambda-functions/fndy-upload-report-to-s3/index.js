// eslint-disable-next-line import/no-unresolved
const AWS = require('aws-sdk');
const atob = require('atob');
const fs = require('fs');
const wkhtmltopdf = require('./src/helpers/wkhtmltopdf');

process.env['PATH'] = process.env['PATH'] + ':' + process.env['LAMBDA_TASK_ROOT'];
process.env.FONTCONFIG_PATH = '/var/task/fonts';

const S3config = { bucketName: process.env.S3_BUCKET }; // Change to your bucket name

exports.handler = (event, context, callback) => {
    // Get the values from the request
    const htmlString = atob(event.body.html_base64);
    const { fileName } = event.body;
    const { options } = event.body;

    // Create the PDF file from the HTML string
    const output = `/tmp/${fileName}.pdf`;
    const writeStream = fs.createWriteStream(output);

    wkhtmltopdf(htmlString, options, (err) => {
        if (err) {
            callback(err);
        }

        const s3 = new AWS.S3();
        const fullname = `pdfs/${fileName}.pdf`;
        const params = {
            Bucket: S3config.bucketName,
            Key: fullname,
            Body: fs.createReadStream(output),
            ContentType: 'application/pdf',
            ACL: 'public-read',
        };

        s3.putObject(params, (errS3Put) => {
            if (errS3Put) {
                const error = new Error(
                    'There was an error while saving the PDF to S3',
                );
                callback(error);
            } else {
                s3.getSignedUrl(
                    'getObject',
                    {
                        Bucket: S3config.bucketName,
                        Key: fullname,
                    },
                    (errS3, url) => {
                        if (err) {
                            return callback(errS3);
                        }

                        return context.done(null, {
                            url: decodeURI(url.split('?')[0]),
                        });
                    },
                );
            }
        });
    }).pipe(writeStream);
};
