const dao = require('foundry-dao/dynamodb')(
    process.env.AWS_REGION,
    `_${process.env.ENV}`,
);

const rdsDao = require('foundry-dao/rds')(
    {
        host: process.env.DB_HOST,
        user: 'search_user',
        port: 5432,
        database: 'AdvancedSearch',
    },
    process.env.AWS_REGION || 'eu-central-1',
    process.env.ENV,
);
const util = require('util');
const AWS = require('aws-sdk');
const asyncc = require('async');
const uuid = require('uuid');

const lambda = new AWS.Lambda();

const DEVICES_SEARCH = {
    criteria: [
        {
            indexValues: {
                typeId: '84bf2c70-ff4f-11e7-a278-770f8c02f05e',
            },
            indexType: 'FNDY_OBJ_TYPE',
            filters: [],
        },
    ],
    openColumns: [
        {
            fndyLink: '/objectManager/detail/',
            identifier: true,
            id: 'a.name',
            label: 'Name',
            type: 'string',
            open: true,
            unhidable: true,
            fndyLinkId: 'deviceId',
        },
        {
            id: 'a.contact.name',
            label: 'Contact Name',
            type: 'string',
            open: true,
        },
        {
            fndyLink: '/accountManager/detail/',
            id: 'accountId',
            label: 'Account',
            type: 'link',
            open: true,
            fndyLinkId: 'accountId',
        },
        {
            id: 'a.fw.version',
            label: 'Device Firmware',
            type: 'string',
            open: true,
        },
        {
            id: 'a.node.ups.name',
            label: 'UPS Name',
            type: 'string',
            open: true,
        },
    ],
    name: 'Devices',
    searchType: 'ObjectSearch',
    viewType: 'List',
    withAction: false,
    withCriteria: true,
};

const TICKETS_SEARCH = {
    criteria: [
        {
            indexValues: {
                typeId: '90dbc7d0-ff62-11e7-a108-db143d1878ff',
            },
            indexType: 'FNDY_OBJ_TYPE',
            filters: [],
        },
    ],
    openColumns: [
        {
            fndyLink: '/objectManager/detail/',
            identifier: true,
            id: 'a.name',
            label: 'Name',
            type: 'string',
            open: true,
            unhidable: true,
            fndyLinkId: 'deviceId',
        },
        {
            id: 'a.description',
            label: 'Description',
            type: 'string',
            open: true,
        },
        {
            id: 'a.device_id',
            label: 'Device',
            type: 'customTemplate',
            open: true,
        },
        {
            id: 'a.status',
            label: 'Status',
            type: 'string',
            open: true,
        },
        {
            fndyLink: '/accountManager/detail/',
            id: 'accountId',
            label: 'Account',
            type: 'link',
            open: true,
            fndyLinkId: 'accountId',
        },
    ],
    name: 'Tickets',
    withAction: false,
    withCriteria: true,
    searchType: 'ObjectSearch',
    viewType: 'List',
};

function connectRds() {
    return util.promisify(rdsDao.connect)();
}

function checkExistingUser(user) {
    if (!user.email) {
        return Promise.reject(
            new Error('An email is required to register a user.'),
        );
    }

    return util
        .promisify(rdsDao.user.getByUsernameInRootAccount)(
            user.email,
            process.env.ACCOUNT_ID,
        )
        .then((existing) => {
            if (existing) {
                throw new Error(
                    `User with email: ${existing.username} already exists.`,
                );
            }
            return false;
        })
        .catch((e) => {
            if (e === 'User not found') {
                return false;
            }
            throw new Error(e);
        });
}

function checkExistingDevice(device) {
    const params = {
        FunctionName: `pre-hook-check-duplicate-serials-${process.env.ENV}`,
        InvocationType: 'RequestResponse',
        Payload: JSON.stringify({
            fndyObject: device,
        }),
    };
    return lambda
        .invoke(params)
        .promise()
        .then((returnVal) => {
            const result = JSON.parse(returnVal.Payload);
            if (result.errorMessage) {
                throw new Error(result.errorMessage);
            }
            return result;
        });
}

function invokeLambda(functionName, payload, context) {
    const params = {
        FunctionName: `${functionName}-${process.env.ENV}`,
        InvocationType: 'RequestResponse',
        Payload: JSON.stringify({
            body: payload,
            query: {},
            params: {},
            invoke: true,
        }),
        ClientContext: Buffer.from(JSON.stringify(context)).toString(
            'base64',
        ),
    };
    return lambda
        .invoke(params)
        .promise()
        .then((returnVal) => {
            const result = JSON.parse(returnVal.Payload);
            if (result.errorMessage) {
                throw new Error(result.errorMessage);
            }
            return result;
        });
}

function sendTempPassword(username, email, context) {
    const params = {
        FunctionName: `fndy-send-password-user-${process.env.ENV}`,
        InvocationType: 'RequestResponse',
        Payload: JSON.stringify({
            body: {
                userName: username,
                email,
            },
            query: {},
            params: {},
            invoke: true,
        }),
        ClientContext: Buffer.from(JSON.stringify(context)).toString(
            'base64',
        ),
    };
    return lambda
        .invoke(params)
        .promise()
        .then((returnVal) => {
            const result = JSON.parse(returnVal.Payload);
            if (result.errorMessage) {
                throw new Error(result.errorMessage);
            }
            return result;
        })
        .catch((e) => {
            throw new Error(e);
        });
}

function createSavedSearches(accountId) {
    const savedSearches = [DEVICES_SEARCH, TICKETS_SEARCH];

    return util.promisify(asyncc.map)(
        savedSearches,
        async (search) => {
            const toCreate = {
                ...search,
                userId: accountId,
                id: uuid.v4(),
            };

            const res = await util.promisify(dao.savedSearch.Create)(
                toCreate,
            );
            return res;
        },
    );
}

function deleteAccount(accountId, context) {
    const params = {
        FunctionName: `fndy-delete-account-secure-${process.env.ENV}`,
        InvocationType: 'RequestResponse',
        Payload: JSON.stringify({
            params: {
                account_id: accountId,
            },
            query: {},
            body: {},
            invoke: true,
        }),
        ClientContext: Buffer.from(JSON.stringify(context)).toString(
            'base64',
        ),
    };
    return lambda
        .invoke(params)
        .promise()
        .then((returnVal) => JSON.parse(returnVal.Payload));
}

function deleteDevice(deviceId, context) {
    const params = {
        FunctionName: `fndy-delete-device-secure-${process.env.ENV}`,
        InvocationType: 'RequestResponse',
        Payload: JSON.stringify({
            params: {
                device_id: deviceId,
            },
            query: {},
            body: {},
            invoke: true,
        }),
        ClientContext: Buffer.from(JSON.stringify(context)).toString(
            'base64',
        ),
    };
    return lambda
        .invoke(params)
        .promise()
        .then((returnVal) => JSON.parse(returnVal.Payload));
}

function deleteUser(userId, context) {
    const params = {
        FunctionName: `fndy-delete-user-secure-${process.env.ENV}`,
        InvocationType: 'RequestResponse',
        Payload: JSON.stringify({
            params: {
                user_id: userId,
            },
            query: {},
            body: {},
            invoke: true,
        }),
        ClientContext: Buffer.from(JSON.stringify(context)).toString(
            'base64',
        ),
    };
    return lambda
        .invoke(params)
        .promise()
        .then((returnVal) => JSON.parse(returnVal.Payload));
}

function createPages(savedSearches, accountId) {
    return util.promisify(asyncc.map)(
        savedSearches,
        async (search) => {
            const toCreate = {
                name: search.name,
                menu_item: 'Asset Management',
                url: `/objectManager/savedSearch/${search.id}`,
                user_role: 'User',
                id: uuid.v4(),
                account_id: accountId,
            };

            const res = await util.promisify(
                rdsDao.portalPage.create,
            )(toCreate);
            return res;
        },
    );
}

function createOtherPages(accountId) {
    const pages = [
        {
            name: 'Devices Location',
            url:
                '/iframe?url=https://d2lxmt98eckj6y.cloudfront.net/toshibaDevicesLocation',
            user_role: 'User',
            id: uuid.v4(),
            account_id: accountId,
        },
        {
            name: 'Service Request',
            url:
                'https://www.toshiba.com/tic/service-warranty/service-request-forms',
            user_role: 'View Only',
            id: uuid.v4(),
            account_id: accountId,
        },
    ];
    return util.promisify(asyncc.map)(pages, async (page) => {
        const res = await util.promisify(rdsDao.portalPage.create)(
            page,
        );
        return res;
    });
}

async function handler(event, context) {
    let createdNewUser = false;
    let createdNewAccount = false;
    let createdNewDevice = false;
    let createdNewGroup = false;
    let userId;
    let accountId;
    let deviceId;
    let groupId;

    try {
        const { user, account, device } = event.body;
        await connectRds();

        if (!user.id) {
            await checkExistingUser(user);
        }

        await checkExistingDevice(device);

        accountId = account.id;
        userId = user.id;

        let createdUser;
        let createdAccount;

        if (!account.id) {
            const newAccount = await invokeLambda(
                'fndy-post-account-secure',
                account,
                context,
            );
            createdNewAccount = true;
            accountId = newAccount.id;
            createdAccount = newAccount;
        }

        const newDevice = await invokeLambda(
            'fndy-post-device-secure',
            { ...device, accountId },
            context,
        );
        createdNewDevice = true;
        deviceId = newDevice.deviceId;

        if (createdNewAccount) {
            const searches = await createSavedSearches(accountId);
            await createPages(searches, accountId);
            await createOtherPages(accountId);
            const newGroup = await invokeLambda(
                'fndy-post-device-secure',
                {
                    accountId,
                    'a.name': 'Tickets',
                    typeId: '321a7270-0fc8-11e8-b684-81698f9d4534',
                    isParent: true,
                },
                context,
            );
            createdNewGroup = true;
            groupId = newGroup.deviceId;
        }

        if (!user.id) {
            const active = user.active ? 1 : 0;
            const userCopy = {
                ...user,
                account_id: accountId,
                active,
            };
            const newUser = await invokeLambda(
                'fndy-post-user-secure',
                userCopy,
                context,
            );
            createdNewUser = true;
            userId = newUser.id;
            createdUser = newUser;
        }

        if (createdNewUser) {
            const firstname = user.first_name || 'Customer';
            const lastname = user.last_name || '';

            await sendTempPassword(
                `${firstname} ${lastname}`,
                user.email,
                context,
            );
        }

        return Promise.resolve({
            user: createdNewUser ? createdUser : user,
            account: createdNewAccount ? createdAccount : account,
            device: newDevice,
        });
    } catch (e) {
        if (createdNewUser) {
            await deleteUser(userId, context);
        }

        if (createdNewAccount) {
            await deleteAccount(accountId, context);
        }

        if (createdNewDevice) {
            await deleteDevice(deviceId, context);
        }

        if (createdNewGroup) {
            await deleteDevice(groupId, context);
        }

        return Promise.reject(e);
    }
}

exports.handler = handler;
