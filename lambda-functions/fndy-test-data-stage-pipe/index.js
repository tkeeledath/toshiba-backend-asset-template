const async = require('async');
const AWS = require('aws-sdk');
const region = process.env.AWS_REGION || 'us-west-2';
exports.handler = function (event, context, callback) {
    console.log('event', event);
    async.waterfall([validation, publishMessage], function (
        err,
        data,
    ) {
        if (err) {
            var response = {
                error: err.error ? err.error : err,
            };
            callback(JSON.stringify(response), null);
        } else {
            callback(null, data);
        }
    });

    function validation(cbValidation) {
        if (!event) {
            return cbValidation('Topic & Body required');
        }
        return cbValidation(null);
    }

    function publishMessage(cbPublishMessage) {
        var iotdata = new AWS.IotData({
            endpoint: process.env.iotEndpoint,
            accessKeyId: process.env.accessKey,
            secretAccessKey: process.env.secretKey,
            region: region,
        });
        const mqtt_topic = event.topic;
        console.log(mqtt_topic);
        delete event.topic;
        var params = {
            topic: mqtt_topic /* required */,
            payload: JSON.stringify(
                event,
            ) /* Strings will be Base-64 encoded on your behalf */,
            qos: 0,
        };
        iotdata.publish(params, function (err, data) {
            if (err) {
                console.log(err, err.stack); // an error occurred
                return cbPublishMessage(err);
            } else {
                console.log(data); // successful response
                return cbPublishMessage(null, data);
            }
        });
    }
};
