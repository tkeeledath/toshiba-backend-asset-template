const dao = require('foundry-dao/dynamodb')(
    process.env.AWS_REGION,
    `_${process.env.ENV}`,
);
const request = require('request');
const asyncc = require('async');

const googleMapsApiKey = process.env.GOOGLE_MAPS_API_KEY;

function getLocationByAddress(oldObject, object, callback) {
    if (
        (oldObject['a.contact.address.street'] &&
            oldObject['a.contact.address.street'] !==
                object['a.contact.address.street']) ||
        (oldObject['a.contact.address.city'] &&
            oldObject['a.contact.address.city'] !==
                object['a.contact.address.city']) ||
        (oldObject['a.contact.address.state'] &&
            oldObject['a.contact.address.state'] !==
                object['a.contact.address.state']) ||
        !object['a.latitude'] ||
        !object['a.longitude']
    ) {
        const address = [
            object['a.contact.address.street'],
            object['a.contact.address.city'],
            object['a.contact.address.state'],
        ].join(',');

        const url = `https://maps.googleapis.com/maps/api/geocode/json?key=${googleMapsApiKey}&address=${encodeURIComponent(
            address,
        )}&language=en`;
        return request.get(
            {
                url,
                json: true,
            },
            (err, response, body) => {
                console.log(err);
                if (err || response.statusCode >= 300)
                    return callback({
                        noChange: true,
                    });
                console.log(body);
                if (!body.results.length) {
                    return callback({
                        noChange: true,
                    });
                }

                return callback(null, {
                    'a.latitude':
                        body.results[0].geometry.location.lat,
                    'a.longitude':
                        body.results[0].geometry.location.lng,
                });
            },
        );
    }
    return callback({
        noChange: true,
    });
}

function handler(event, context) {
    const { oldObject, object } = event;

    const decodedOld = JSON.parse(Buffer.from(oldObject, 'base64'));
    const decodedNew = JSON.parse(Buffer.from(object, 'base64'));

    asyncc.waterfall(
        [
            (cb) => {
                getLocationByAddress(decodedOld, decodedNew, cb);
            },
            (obj, cb) => {
                const { deviceId } = decodedNew;

                console.log(obj);
                dao.deviceInfo.Update(deviceId, obj, cb);
            },
        ],
        (err, result) => {
            if (err && err.noChange) {
                return context.succeed();
            }
            if (err) {
                return context.fail(err);
            }
            return context.succeed(result);
        },
    );
}

exports.handler = handler;
