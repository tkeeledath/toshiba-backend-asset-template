let env = 'dev';

if (
    typeof process.env.ENV === 'string' &&
    (process.env.ENV.toLowerCase() === 'prod' ||
        process.env.ENV.toLowerCase() === 'stage')
) {
    env = process.env.ENV.toLowerCase();
}

const region = process.env.AWS_REGION || 'us-west-2';
const dao = require('foundry-dao/dynamodb')(
    process.env.AWS_REGION || 'eu-central-1',
    `_${env}`,
);
const asyncc = require('async');
const moment = require('moment');
const uuid = require('node-uuid');
const AWS = require('aws-sdk');

const sns = new AWS.SNS({
    region,
});
const nodemailer = require('nodemailer');

const ticketTypeId = process.env.TICKET_TYPE_ID;

const https = require('https');

const dbConfig = {
    region: process.env.AWS_REGION,
    httpOptions: {
        agent: new https.Agent({
            ciphers: 'ALL',
            secureProtocol: 'TLSv1_method',
        }),
    },
};

const dynamoDB = require('@awspilot/dynamodb')(dbConfig);

function CreateTickets(device, events, cb) {
    const faultEvents = events.filter(
        (e) => e.type === 'fault' || e.type === 'critical',
    );

    if (!faultEvents.length) {
        return cb(null);
    }

    console.log('create tickets');

    asyncc.map(
        faultEvents,
        (event, cbMap) => {
            const ticketNumber = device.deviceId;

            const eventObj = {
                deviceId: uuid.v1(),
                accountId: device.accountId,
                typeId: ticketTypeId,
                'a.device_id': device.deviceId,
                'a.description': event.event,
                'a.status': 'OPEN',
                'a.name': `Ticket #${ticketNumber}`,
            };

            return dao.deviceInfo.Create(eventObj, (err) => {
                console.log(err);
                const ref = {
                    refFrom: eventObj.deviceId,
                    refTo: device.deviceId,
                };
                dao.objectReference.Create(ref, () => {
                    cbMap();
                });
            });
        },
        (err) => {
            cb(err);
        },
    );
}

function CreateEventInDB(device, events, cbCreateEventInDB) {
    console.log('Create in DynamoDB');

    asyncc.map(
        events,
        (event, cb) => {
            const eventCopy = { ...event };

            eventCopy.epochTime = moment().valueOf();
            eventCopy.deviceId = device.deviceId;
            dynamoDB
                .table(`toshiba_events_${env}`)
                .insert_or_update(eventCopy, (err) => {
                    if (err) {
                        console.error(err);
                        return cb(null);
                    }

                    return cb(null);
                });
        },
        () => cbCreateEventInDB(null),
    );
}

function UpdateEventStatus(device, events, cbUpdateEventStatus) {
    console.log('Update Event Status');

    events.sort((a, b) => {
        if (a.epochtime > b.epochtime) return 1;
        if (a.epochtime < b.epochtime) return -1;
        return 0;
    });

    const lastEventType = events[events.length - 1].type;

    if (
        !device['a.last_event_type'] ||
        device['a.last_event_type'] !== lastEventType
    ) {
        dao.deviceInfo.Update(
            device.deviceId,
            { 'a.last_event_type': lastEventType },
            () => cbUpdateEventStatus(null),
        );
    } else {
        cbUpdateEventStatus(null);
    }
}

function PublishEventsToMqtt(device, events, cbPublishEventsToMqtt) {
    const iotdata = new AWS.IotData({
        region,
        endpoint: process.env.IOT_ENDPOINT,
    });

    asyncc.map(
        events,
        (event, cbMap) => {
            const mqttparams = {
                topic: `toshiba/${device.deviceId}/events`,
                payload: JSON.stringify(event),
                qos: 0,
            };

            iotdata.publish(mqttparams, () => {
                cbMap(null);
            });
        },
        () => cbPublishEventsToMqtt(null),
    );
}

function SendEmailNotification(device, events, cbSendNotification) {
    console.log('Send EMAIL Notifications');

    const toEmails = [process.env.TO_EMAIL] || [];

    if (
        device['a.receive_notifications'] &&
        device['a.receive_notifications'] === 'true' &&
        device['a.contact.email']
    ) {
        console.log('emails!');
        toEmails.push(device['a.contact.email']);
    }

    console.log(toEmails);

    if (!toEmails.length) {
        return cbSendNotification(null);
    }

    let html = `New events for device:<br/><br/>
          Name: ${device['a.name']}<br/>
          Account: ${device.accountId}<br/>
          Device ID: ${device.deviceId}<br/>
          Device Serial Number: ${device['a.serial']}<br/>
          UPS Serial Number: ${
              device['a.node.ups.serial'] || 'n/a'
          }<br/>
          UPS model: ${device['a.node.ups.model'] || 'n/a'}<br/>
          Load Status: ${device['a.load_status'] || 'n/a'}<br/>
          Battery Status: ${device['a.battery_status'] || 'n/a'}<br/>
          Input Voltage: ${
              device['a.node.ups.input.voltage1'] !== undefined
                  ? device['a.node.ups.input.voltage1']
                  : 'n/a'
          }<br/>
          Output Voltage: ${
              device['a.node.ups.output.voltage1'] !== undefined
                  ? device['a.node.ups.output.voltage1']
                  : 'n/a'
          }<br/>
          Input Frequensy: ${
              device['a.node.ups.input.frequency1'] !== undefined
                  ? device['a.node.ups.input.frequency1']
                  : 'n/a'
          }<br/>
          Output Load (%): ${
              device['a.node.ups.output.loadpercentage'] !== undefined
                  ? device['a.node.ups.output.loadpercentage']
                  : 'n/a'
          }<br/>
          Battery Capacity (%): ${
              device['a.node.ups.battery.capacity'] !== undefined
                  ? device['a.node.ups.battery.capacity']
                  : 'n/a'
          }<br/>
          Battery Voltage (V): ${
              device['a.node.ups.battery.voltage'] !== undefined
                  ? device['a.node.ups.battery.voltage']
                  : 'n/a'
          }<br/>
          Battery Temp. (C): ${
              device['a.node.ups.temperature.battery'] !== undefined
                  ? device['a.node.ups.temperature.battery']
                  : 'n/a'
          }<br/><br/>
          Events:
          <table style="border-collapse: collapse">
              <thead>
                  <tr>
                      <th>Severity</th>
                      <th>Time</th>
                      <th>Message</th>
                  </tr>
              </thead>
              <tbody>`;

    const severity = {
        info: 'Informational',
        information: 'Informational',
        warning: 'Warning',
        fault: 'Fault',
        critical: 'Fault',
    };
    events.forEach((event) => {
        html += `<tr>
                      <td style="border: solid 1px">${
                          severity[event.type]
                      }</td>
                      <td style="border: solid 1px">${moment(
                          event.epochtime * 1000,
                      ).format('MM/DD/YYYY hh:mm A')}</td>
                      <td style="border: solid 1px">${event.node.toUpperCase()}: ${
            event.event
        }</td>
                  </tr>`;
    });

    html += `</tbody>
          </table>`;

    const mailOptions = {
        from: process.env.FROM_EMAIL || 'TIC-UPSService@toshiba.com',
        to: toEmails,
        subject:
            process.env.SUBJECT_EMAIL ||
            'New Event on RemotEye device',
        html,
    };
    AWS.config.endpoint = 'email.us-east-1.amazonaws.com';
    const smtpTransport = nodemailer.createTransport({
        SES: new AWS.SES({
            region: 'us-east-1',
        }),
    });
    smtpTransport.sendMail(mailOptions, () =>
        cbSendNotification(null),
    );
}

function SendSmsNotification(device, events, cbSendSmsNotifications) {
    if (
        !device['a.receive_sms_notifications'] ||
        device['a.receive_sms_notifications'] !== 'true' ||
        !device['a.notification_phone_numbers']
    )
        return cbSendSmsNotifications(null);

    console.log('Send SMS Notifications');

    const phoneNumbers = device['a.notification_phone_numbers'].split(
        ',',
    );

    if (!phoneNumbers.length) return cbSendSmsNotifications(null);

    let message = `${device['a.name']}: `;

    message += events.map((e) => e.event).join(', ');
    // events.forEach((event) => {
    //     message += `${event.event},`;
    // });
    message += `\nSerial: ${device['a.serial']}\nUPS Serial: ${device['a.node.ups.serial']}`;

    asyncc.map(
        phoneNumbers,
        (number, cb) => {
            let numberCopy = number;

            numberCopy = numberCopy.replace(/[^0-9+]/g, '');
            if (number.length === 10) {
                numberCopy = `+1${numberCopy}`;
            }

            console.log(numberCopy);
            const params = {
                Message: message.substring(0, message.length - 1),
                MessageStructure: 'string',
                PhoneNumber: numberCopy,
                MessageAttributes: {
                    SMSType: {
                        DataType: 'String',
                        StringValue: 'Transactional',
                    },
                    SenderID: {
                        DataType: 'String',
                        StringValue: 'Toshiba',
                    },
                },
            };

            sns.publish(params, (err, data) => {
                console.log(err, data);
                cb(null);
            });
        },
        () => {
            cbSendSmsNotifications(null);
        },
    );
}

function handler(event, context) {
    const { device, events } = event;

    console.log(device);
    console.log(event);

    if (!device || !events || !events.length) {
        return context.fail('No events to process');
    }

    return asyncc.waterfall(
        [
            (cb) => {
                CreateTickets(device, events, cb);
            },
            (cb) => {
                CreateEventInDB(device, events, cb);
            },
            (cb) => {
                UpdateEventStatus(device, events, cb);
            },
            (cb) => {
                PublishEventsToMqtt(device, events, cb);
            },
            (cb) => {
                SendEmailNotification(device, events, cb);
            },
            (cb) => {
                SendSmsNotification(device, events, cb);
            },
        ],
        (err, result) => {
            if (err) {
                console.error('error:', err);
                const response = {
                    status: 400,
                    error: err.message ? err.message : err,
                };
                return context.succeed(JSON.stringify(response));
            }

            return context.succeed(result);
        },
    );
}

exports.handler = handler;
