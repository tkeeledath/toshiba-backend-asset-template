BEGIN; 

DELETE FROM "fndy_portal_page" WHERE url = '/mqttClient' OR url = '/profile' OR url = '/logout' OR url = '/awsCertificates' OR url = '/userLocation' OR url = '/groups' OR url = '/iotRules' OR url = '/provisioningRules' OR url = '/metrics';

UPDATE "fndy_portal_page" SET url = '/iframe?url=https://d2lxmt98eckj6y.cloudfront.net/reporting' WHERE url = '/toshibaReporting';

UPDATE "fndy_portal_page" SET url = '/iframe?url=https://d2lxmt98eckj6y.cloudfront.net/toshibaDevicesLocation' WHERE url = '/toshibaDevicesLocation';

UPDATE "fndy_portal_page" SET url = '/iframe?url=https://d2lxmt98eckj6y.cloudfront.net/certificateProvisioning' WHERE url = '/certificateProvisioning';

UPDATE "fndy_portal_page" SET url = '/iframe?url=https://d2lxmt98eckj6y.cloudfront.net/registerToshibaUser' WHERE url = '/registerToshibaUser';


UPDATE "fndy_portal_page" SET url = '/objectManager' WHERE url = '/devices' OR url = '/advancedSearchManagerNew';

UPDATE "fndy_portal_page" SET url = '/objectTypeManager' WHERE url = '/deviceTypeManager';

UPDATE "fndy_portal_page" SET url = '/accountManager/create' WHERE url = '/createSubAccount';

UPDATE "fndy_portal_page" SET url = '/userManager/create' WHERE url = '/createUser';

UPDATE "fndy_portal_page" SET url = '/userManager' WHERE url = '/manageUsers';

UPDATE "fndy_portal_page" SET url = '/settings?tab=general' WHERE url = '/preferences';

UPDATE "fndy_portal_page" SET url = '/objectManager/savedSearch/3f2a0f90-90ba-11eb-b11d-1158135e0e26' WHERE name = 'Service Ticket Management';

UPDATE "fndy_portal_page" SET url = '/objectManager/savedSearch/ca604b70-90b9-11eb-b11d-1158135e0e26' WHERE name = 'Devices' OR name = 'Toshiba Devices';

COMMIT;
